package com.qytest.springcloud.controller;

import com.qytest.springcloud.entities.CommonResult;
import com.qytest.springcloud.entities.Order;
import com.qytest.springcloud.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("")
@Slf4j
public class OrderController {
    @Resource
    private OrderService orderService;

//   @GetMapping("/order/create")
//   public CommonResult create(Order order)
//   {
//       orderService.create(order);
//       return new CommonResult(200,"订单创建成功");
//   }

    @GetMapping("/order/create")
    public CommonResult<Order> create(Order order) {
        orderService.create(order);
        return new CommonResult<Order>(200, "订单创建成功", order);
    }

}
