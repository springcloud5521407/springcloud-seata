package com.qytest.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qytest.springcloud.entities.Order;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
public interface OrderService extends IService<Order> {

    void create(Order order);

}