package com.qytest.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qytest.springcloud.dao.OrderMapper;
import com.qytest.springcloud.entities.Order;
import com.qytest.springcloud.service.AccountService;
import com.qytest.springcloud.service.OrderService;
import com.qytest.springcloud.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author qy
 * @date 2022年07月8日 15:38
 */
@Service
@Slf4j
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

        @Resource
        private StorageService storageService;
        @Resource
        private AccountService accountService;

        /**
         * 创建订单->调用库存服务扣减库存->调用账户服务扣减账户余额->修改订单状态
         * 简单说：下订单->扣库存->减余额->改状态
         */
        @Override
        public void create(Order order) {
            //1 新建订单
            log.info("----->开始新建订单");
            baseMapper.create(order);
            log.info("----->新建订单完成");

            //2 扣减库存
            log.info("----->订单微服务开始调用库存，做扣减Count");
            storageService.decrease(order.getProductId(), order.getCount());
            log.info("----->库存扣减Count完成");

            //3 扣减账户
            log.info("----->订单微服务开始调用账户，做扣减Money");
            accountService.decrease(order.getUserId(), order.getMoney());
            log.info("----->账户扣减Money完成");

            //4 修改订单状态，从0到1,1代表已经完成
            log.info("----->修改订单状态开始");
            baseMapper.update(order.getUserId(),0);
            log.info("----->修改订单状态结束");

            log.info("----->下订单结束了，O(∩_∩)O哈哈~");

        }
}