package com.qytest.springcloud.service;

import com.qytest.springcloud.entities.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
@FeignClient(value = "seata-storage-service")
public interface StorageService {
    //请求方式要与2002中的StorageController一致
    @PostMapping(value = "/storage/decrease")
    CommonResult decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);
}