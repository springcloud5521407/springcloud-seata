package com.qytest.springcloud.service;

import com.qytest.springcloud.entities.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
@FeignClient(value = "seata-account-service")
public interface AccountService {
    //请求方式要与2003中的AccountController一致
    @PostMapping(value = "/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}