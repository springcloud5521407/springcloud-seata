package com.qytest.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qytest.springcloud.entities.Order;
import org.apache.ibatis.annotations.Param;

/**
 * @author qy
 * @date 2022年07月08日 17:56
 */
public interface OrderMapper extends BaseMapper<Order> {
    //1、新建订单
    void create(@Param("order") Order order);

    //2、修改订单状态，从零改为1
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}
