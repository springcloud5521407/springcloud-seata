package com.qytest.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qytest.springcloud.entities.Storage;
import org.apache.ibatis.annotations.Param;

/**
 * @author qy
 * @date 2022年07月08日 17:56
 */
public interface StorageMapper extends BaseMapper<Storage> {
    //扣减库存
    void decrease(@Param("productId") Long productId, @Param("count") Integer count);
}
