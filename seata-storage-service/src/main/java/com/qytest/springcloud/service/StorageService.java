package com.qytest.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qytest.springcloud.entities.Storage;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
public interface StorageService extends IService<Storage> {
    //扣减库存
    void decrease(Long productId, Integer count);
}