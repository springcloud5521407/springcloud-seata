package com.qytest.springcloud.controller;

import com.qytest.springcloud.entities.CommonResult;
import com.qytest.springcloud.service.StorageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StorageController {
    @Resource
    private StorageService storageService;

    //扣减库存
    //请求方式要与2001中的StorageService一致
    //想取巧的话可以写
    //@RequestMapping("/storage/decrease")
    //交给Spring MVC自动判断
    @PostMapping(value = "/storage/decrease")
    public CommonResult decrease(Long productId, Integer count){
        storageService.decrease(productId, count);
        return new CommonResult(200,"扣减库存成功！");
    }
}
