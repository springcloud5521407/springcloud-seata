package com.qytest.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qytest.springcloud.entities.Account;

import java.math.BigDecimal;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
public interface AccountService extends IService<Account> {
    //扣减账户余额
    void decrease(Long userId, BigDecimal money);
}