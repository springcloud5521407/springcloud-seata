package com.qytest.springcloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qytest.springcloud.entities.Account;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @author qy
 * @date 2022年07月08日 17:56
 */
public interface AccountMapper extends BaseMapper<Account> {
    //扣减账户余额
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
